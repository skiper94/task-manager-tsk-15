package ru.apolyakov.tm.api.enumerated;

import ru.apolyakov.tm.comparator.ComparatorByCreated;
import ru.apolyakov.tm.comparator.ComparatorByDateStart;
import ru.apolyakov.tm.comparator.ComparatorByName;
import ru.apolyakov.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by date start", ComparatorByDateStart.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String dispayName;

    private final Comparator comparator;

    Sort(String dispayName, Comparator comparator) {
        this.dispayName = dispayName;
        this.comparator = comparator;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDispayName() {
        return dispayName;
    }

}
