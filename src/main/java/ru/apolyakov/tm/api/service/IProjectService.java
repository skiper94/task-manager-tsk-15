package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.api.enumerated.Status;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project removeOneByName(String name);

    Project removeOneById(String id);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project removeProjectByIndex(Integer index);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project finishProjectById(String id);

    Project finishProjectByIndex(Integer index);

    Project finishProjectByName(String name);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusByName(String name, Status status);

}
