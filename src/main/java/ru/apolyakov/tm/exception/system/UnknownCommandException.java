package ru.apolyakov.tm.exception.system;

import ru.apolyakov.tm.constant.TerminalConst;
import ru.apolyakov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public  UnknownCommandException() {
        super("Incorrect command. For show command use \"" + TerminalConst.CMD_HELP + "\"");
    }

}
